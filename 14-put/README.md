# Outline : #14 PUT 

- Membuat file put-warga-by-id.js 
- Mengimport db pool
- Membuat fungsi export sebagai modul
- Mendapatkan query parameter dari url dengan request.query.nama_parameter
- Mendapatkan PUT JSON body dengan request.body()  
- Fungsi pool.query() untuk eksekusi query SQL
- Query SQL data warga
- Tes API dengan Hoppscotch
