# Outline : #8 Arsitektur Aplikasi 

Berdasarkan kode aplikasi Express Hello World yang dibuat di video sebelumnya

1. Mengapa butuh arsitektur aplikasi ? 
  - Memudahkan penambahan dan pembaruan fungsi aplikasi 
  - Memudahkan identifikasi kesalahan pada aplikasi 
  - Memudahkan kolaborasi tim dalam pengembangan aplikasi
  - Memudahkan pembuatan aplikasi baru

2. Routes pada pengembangan web 
  - Mengelola Response dari alamat URL yang diRequest oleh client
  - Response dapat berupa
    - Halaman web seperti HTML
    - RESTful API berformat JSON
    - Media seperti mp3, mp4, pdf, zip, dsb.
  - Response dapat diambil dari
    - Resource statik: 
      - Contohnya file HTML, JSON, png, zip, dsb.
    - Resource dinamis:
      - Contohnya membuat Response dinamis menggunakan bahasa Python dengan output file HTML, JSON, png, zip, dsb. 

3. Arsitektur Routes - Services 
  - Sederhana
  - Routes menangani pembacaan URL dan menentukan Service mana yang akan memberi Response
  - Services memberikan Response dari URL sesuai yang diberikan Routes
    - Eksekusi logika proses bisnis
    - Koneksi ke database dan query database
    - Memberikan output Response seperti JSON, HTML atau media

4. Pembuatan folder routes, services, utils
5. Pembuatan file routes
  - warga.js
    ```js 
        const express = require('express')
        const router = express.Router()

        module.exports = router
    ```
  - aspirasi.js
  - index.js
6. CORS 
7. JSON body input
8. Mengimport routes ke app.js
  ```js 
    const indexRoutes = require('./routes/index')
    const aspirasiRoutes = require('./routes/aspirasi')
    const wargaRoutes = require('./routes/warga')
  ```
9. Memasukan routes yang diimport ke dalam aplikasi
  ```js

  app.use("", indexRoutes)
  app.use("/aspirasi", aspirasiRoutes)
  app.use("/warga", wargaRoutes)
  ```
10. Menggunakan express.json() untuk menangkap JSON body saat PUT dan POST
  ```js
  app.use(express.json())
  ```
