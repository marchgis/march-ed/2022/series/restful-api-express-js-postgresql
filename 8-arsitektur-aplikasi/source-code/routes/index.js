const express = require('express')
const router = express.Router()

// GET /token
// GET /health-check
// GET /

router.get('/', (httpRequest, httpResponse) => {
  
  httpResponse.json({
    hello: "Dunia !"
  })
})

module.exports = router
