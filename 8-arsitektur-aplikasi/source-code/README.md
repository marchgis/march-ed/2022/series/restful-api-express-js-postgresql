# Arsitektur Aplikasi 

1. Mengapa butuh arsitektur aplikasi ? 
  - Memudahkan penambahan dan pembaruan fungsi aplikasi 
  - Memudahkan identifikasi kesalahan pada aplikasi 
  - Memudahkan kolaborasi tim dalam pengembangan aplikasi
  - Memudahkan pembuatan aplikasi baru

2. Routes pada pengembangan web 
  - Mengelola siapa yang akan memberikan Response dari alamat URL yang diRequest oleh client
  - Response dapat berupa
    - Halaman web seperti HTML
    - RESTful API berformat JSON
    - Media seperti mp3, mp4, pdf, zip, dsb.
  - Response dapat diambil dari
    - Resource statik: 
      - Contohnya file HTML, JSON, png, zip, dsb.
    - Resource dinamis:
      - Contohnya membuat Response dinamis menggunakan bahasa Python dengan output file HTML, JSON, png, zip, dsb. 

3. Arsitektur Routes - Services 
  - Sederhana
  - Routes menangani pembacaan URL dan menentukan Service mana yang akan memberi Response
  - Services memberikan Response dari URL sesuai yang diberikan Routes
    - Eksekusi logika proses bisnis
    - Koneksi ke database dan query database
    - Memberikan output Response seperti JSON, HTML atau media
