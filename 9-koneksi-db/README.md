# Outline : #9 Koneksi ExpressJS ke Database 

1. Koneksi database dengan package node-postgres (pg)
2. node-postgres secara otomatis akan memeriksa keberadaan environment variable
  PGUSER=???
  PGHOST=???
  PGDATABASE=???
  PGPASSWORD=???
  PGPORT=???
2. Menggunakan environment variable
3. Pemanggilan environment variable dari script NodeJS 
4. dotenv.config()
5. Pool otomatis menggunakan environment variable 
PGUSER=postgres
PGHOST=localhost
PGDATABASE=aspirasi_warga
PGPASSWORD=tujuanhidupmu
PGPORT=5432

Cek koneksi dengan :

const { Client } = require('pg')

const indexRoutes = require('./routes/index')
const aspirasiRoutes = require('./routes/aspirasi')
const wargaRoutes = require('./routes/warga')

const dotenv = require('dotenv')
dotenv.config()

const client = new Client()
client.connect()

6. Membuat file berisi pool

7. Penjelasan database Pool
8. Penjelasan penggunaan Pool 
9. Parameterized query
