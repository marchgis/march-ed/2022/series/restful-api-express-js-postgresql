# Outline : #7 Struktur Data dengan UUID 

1. Use case aplikasi Aspirasi Warga 
  - Warga dapat mengirimkan aspirasi
  - Warga dapat membaca aspirasi dari warga lainnya
  - Admin dapat mengelola data warga 
2. Entitas aplikasi
  - Warga
    - id
    - nik
    - nama
    - password
    - tipe user
    - alamat
    - rt
    - rw
    - kelurahan
    - foto
  - Aspirasi
    - id
    - pengusul 
    - kategori aspirasi
    - isi aspirasi
    - foto pendukung
3. UUID
  - Universally Unique Identifier
    - Pengidentifikasi unik sedunia ga ada yang nyamain
  - Terdiri dari 128 bits karakter
    - Atau 32 karakter
    - Ditambahkan 4 tanda penghubung
    - Total 36 karakter
  - Dibuat berdasarkan kombinasi komponen-komponen unik seperti
    - Waktu saat ini dalam nanosecond
    - Bilangan acak 
    - Kata kunci
    - Alamat unik dari komputer 
  - Ada versi 1, 2, 3, 4, 5 masing-masing memiliki komponen yang berbeda-beda
  - Umum digunakan pada
    - ID data  
    - URL halaman web
    - Token
    - Penamaan dokumen
    - Penamaan folder
  - Kegunaan UUID  
    - Memastikan tidak ada kesamaan ID dalam tabel yang berbeda
    - Mengamankan URL dari coba-coba akses ID berururut 
  - Cara pembuatan UUID
    - via library bahasa pemrograman
    - via fungsi atau ekstensi di database

4. Pembuatan tabel
  Membuat database *aspirasi_warga*
  ```sql
CREATE DATABASE aspirasi_warga;
  ```

  Membuat schema *app* di database *aspirasi_warga*
  ```sql
CREATE SCHEMA app;
  ```

  Memasang ekstensi uuid-ossp untuk fungsi pembuatan UUID
  ```sql
CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
  ```

  Membuat tabel *warga* dan *aspirasi* yang mewakili Warga dan Aspirasi dari Warga
  ```sql
CREATE TABLE app.warga (
  id uuid NOT NULL DEFAULT app.uuid_generate_v4(),
  nik varchar(16) NOT NULL,
  nama varchar(120) NOT NULL,
  passwd varchar(80) NOT NULL,
  tipe_user smallint NOT NULL,
  alamat text NULL,
  foto varchar(240) NULL,
  created_time timestamp NOT NULL DEFAULT now(),
  rt varchar(10) NULL,
  rw varchar(10) NULL,
  kode_kelurahan varchar(16) NOT NULL DEFAULT '3213030001'::character varying,
  CONSTRAINT warga_pk PRIMARY KEY (id)
);

CREATE TABLE app.aspirasi (
  id uuid NOT NULL DEFAULT app.uuid_generate_v4(),
  pengusul uuid NOT NULL,
  kategori SMALLINT,
  isi TEXT,
  foto_pendukung JSONB,
  created_time TIMESTAMP NOT NULL DEFAULT now(),
  created_by uuid NOT NULL, 
  CONSTRAINT aspirasi_pk PRIMARY KEY (id)
);
  ```

  Menghubungkan column *pengusul* pada tabel *aspirasi* dengan column *id* pada tabel *warga* melalui FOREIGN KEY
  ```sql
ALTER TABLE app.aspirasi ADD CONSTRAINT aspirasi_fk FOREIGN KEY (pengusul) REFERENCES app.warga(id);
  ```

  [Kode Wilayah se Indonesia (BPS dan Kemendagri)](https://sig.bps.go.id/bridging-kode/index)
5. Inisiasi sampel data warga 
  ```sql
INSERT INTO app.warga(nik, nama, passwd, tipe_user, alamat, foto, rt, rw)
VALUES
  ('3213990000000001', 'Perbu Ruankoru', '$2a$12$bDCr3Y27XYQ/Mn93gXruM.sgkfg4cn5umo6MnAW2LsQBhkxlUe6uO', 2, 'Lembah No. 33', NULL, '01', '01')
  ,('3213990000000002', 'Ren Dahkansu', '$2a$12$1r4S3V31jH85XYMwRcFXGezVZT/Ln9aiSiXLm/YgatKOost3BpEoG', 2, 'Kampung Cuit 5A', NULL, '01', '03')
  ```
