## Outline

Untuk adek-adeku kelas 5 SD, dan teman-teman terbaiku para mahasiswa/i pembelajar 

RESTful API Dengan Express dan PostgreSQL (Untuk kelas 5 SD)

Playlist Order | Title | Series | Priority
--- | --- | --- | ---
1 | Apa itu RESTful API ? | QnA Web Dev | 80
2 | Apa itu Express ? | QnA Web Dev | 60 
3 | Apa itu PostgreSQL ? | QnA Database | 70 
4 | Cara Install NodeJS | Petunjuk Instalasi | 50 
5 | Cara Install PostgreSQL | Petunjuk Instalasi | 50 
6 | [Inisiasi Project dan Instalasi Package Pendukung](https://youtu.be/xIBXRPfMUCg) | RESTful API - Express PostgreSQL | 90 
7 | [Struktur Data dengan UUID](https://www.youtube.com/watch?v=WIt0vsztR3I) | RESTful API - Express PostgreSQL | 90 
8 | [Arsitektur Aplikasi](https://www.youtube.com/watch?v=cPpKR45z58s) | RESTful API - Express PostgreSQL | 90 
9 | [Koneksi ke Database](https://youtu.be/Qbata_QpQqM) | RESTful API - Express PostgreSQL | 90 
10 | [GET dan URL Query Parameter](https://youtu.be/f_y8-72dYg4) | RESTful API - Express PostgreSQL | 90 
11 | [GET by ID](https://www.youtube.com/watch?v=2_6nzhiIpos) | RESTful API - Express PostgreSQL | 90 
12 | [POST dengan Bcrypt Password](https://www.youtube.com/watch?v=-I8NN0zbQwY) | RESTful API - Express PostgreSQL | 90 
13 | DELETE | RESTful API - Express PostgreSQL | 90 
14 | PUT | RESTful API - Express PostgreSQL | 90 
15 | Autentikasi dan Autorisasi JWT | RESTful API - Express PostgreSQL | 40
16 | Upload File | RESTful API - Express PostgreSQL | 40
17 | Sequelize ORM | RESTful API - Express PostgreSQL | 40
18 | Pembuatan Model ORM | RESTful API - Express PostgreSQL | 40
19 | ORM GET (Aspirasi) | RESTful API - Express PostgreSQL | 40
20 | ORM POST | RESTful API - Express PostgreSQL | 40
21 | ORM PUT | RESTful API - Express PostgreSQL | 40
22 | ORM DELETE | RESTful API - Express PostgreSQL | 40

## Data

### Database
Untuk instalasi PostgreSQL, teman-teman dapat memilih satu dari beberapa metode instalasi berikut ini:
#### Instalasi PostgreSQL Windows via ERB

#### Instalasi PostgreSQL via Docker
```sh
docker run --name expressjs-pg -e POSTGRES_PASSWORD=tujuanhidupmu -p 5432:5432 postgres
```

#### Instalasi PostGIS via Docker
```sh
docker run --name expressjs-pgis -e POSTGRES_PASSWORD=tujuanhidupmu -p 5432:5432 postgis/postgis
```

### DDL

Membuat database *warga*
```sql
CREATE DATABASE warga;
```

Membuat schema *app* di database *warga*
```sql
CREATE SCHEMA app;
```

Memasang ekstensi uuid-ossp untuk fungsi pembuata UUID
```sql
CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
```

Membuat tabel *warga* dan *aspirasi* yang mewakili Warga dan Aspirasi dari Warga
```sql
CREATE TABLE app.warga (
  id uuid NOT NULL DEFAULT app.uuid_generate_v4(),
  nik varchar(16) NOT NULL,
  nama varchar(120) NOT NULL,
  passwd varchar(80) NOT NULL,
  tipe_user smallint NOT NULL,
  alamat text NULL,
  foto varchar(240) NULL,
  created_time timestamp NOT NULL DEFAULT now(),
  rt varchar(10) NULL,
  rw varchar(10) NULL,
  kode_kelurahan varchar(16) NOT NULL DEFAULT '3213030001'::character varying,
  CONSTRAINT warga_pk PRIMARY KEY (id)
);

CREATE TABLE app.aspirasi (
  id uuid NOT NULL DEFAULT app.uuid_generate_v4(),
  pengusul uuid NOT NULL,
  kategori SMALLINT,
  isi TEXT,
  foto_pendukung JSONB,
  created_time TIMESTAMP NOT NULL DEFAULT now(),
  created_by uuid NOT NULL, 
  CONSTRAINT aspirasi_pk PRIMARY KEY (id)
);
```

Menghubungkan column *pengusul* pada tabel *aspirasi* dengan column *id* pada tabel *warga* melalui FOREIGN KEY
```sql
ALTER TABLE app.aspirasi ADD CONSTRAINT aspirasi_fk FOREIGN KEY (pengusul) REFERENCES app.warga(id);
```

Inisiasi sampel data warga
  ```sql
INSERT INTO app.warga(nik, nama, passwd, tipe_user, alamat, foto, rt, rw)
VALUES
  ('3213990000000001', 'Perbu Ruankoru', '$2a$12$bDCr3Y27XYQ/Mn93gXruM.sgkfg4cn5umo6MnAW2LsQBhkxlUe6uO', 2, 'Lembah No. 33', NULL, '01', '01')
  ,('3213990000000002', 'Ren Dahkansu', '$2a$12$1r4S3V31jH85XYMwRcFXGezVZT/Ln9aiSiXLm/YgatKOost3BpEoG', 2, 'Kampung Cuit 5A', NULL, '01', '03')
  ```


[Kode Wilayah se Indonesia (BPS dan Kemendagri)](https://sig.bps.go.id/bridging-kode/index)

## NPM Package
- ExpressJS
- CORS
- node-postgres (pg)
- dotenv
- bcryptjs
- sequelize

### Instalasi semua package
```js
npm install express cors pg dotenv bcryptjs sequelize --save
```

## Tools
- nodemon : Mengeksekusi script Javascript, mereload script jika terjadi perubahan pada script tersebut
- dbeaver : Mengeksplorasi dan mengelola berbagai jenis database (PostgreSQL, MySQL, SQL Server, Oracle, Elasticsearch, Apache Hive, dsb.)
- hoppscotch : HTTP client untuk menguji request dan response dari RESTful API
- wrk : Menguji performa RESTful API

## Referensi
- https://developer.mozilla.org/en-US/docs/Learn/Server-side/Express_Nodejs/routes
- https://blog.logrocket.com/the-perfect-architecture-flow-for-your-next-node-js-project/
- https://aws.amazon.com/what-is/restful-api/ 


