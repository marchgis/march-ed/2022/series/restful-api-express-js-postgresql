# Outline : #5 GET 

- Membuat folder services untuk warga
- Membuat file get-warga.js 
- Mengimport db pool
- Membuat fungsi export sebagai modul
- Fungsi pool.query() untuk eksekusi query SQL
- Query SQL mengambil semua data warga
- Tes API dengan Hoppscotch
- Akses query parameter dengan req.query.nama_parameter 
  - RW = 05
- Query SQL jika hanya warga RW 05 yang diambil
- Tes API dengan Hoppscotch
