# Outline : #6 GET by id

- Membuat file get-warga-by-id.js 
- Mengimport db pool
- Membuat fungsi export sebagai modul
- Mendapatkan query parameter dari url dengan request.query.nama_parameter
- Fungsi pool.query() untuk eksekusi query SQL
- Query SQL data warga berdasarkan ID
- Tes API dengan Hoppscotch
