# Outline : #6 Inisiasi Project dan Instalasi Package

## Video

[![Inisiasi Project dan Instalasi Package](https://img.youtube.com/vi/xIBXRPfMUCg/0.jpg)](https://www.youtube.com/watch?v=xIBXRPfMUCg)

## Outline

1. Pastikan NodeJS dan NPM terinstall
2. Buat folder project
3. Masuk ke folder project
4. Inisialisasi project NodeJS dengan npm init
5. Package Nodemon | penggunaan dan instalasi
6. Package Express | penggunaan dan instalasi 
7. Package CORS | penggunaan dan instalasi 
8. Package dotenv | penggunaan dan instalasi 
9. Package node-postgres | penggunaan dan instalasi 
10. Hello world ExpressJS 

## Deskripsi 

Inisiasi project RESTful API menggunakan Express dan PostgreSQL

Library yang digunakan :
1. Express : Framework web untuk membuat web service ataupun web page
2. cors : Perizinan client mana saja yang dapat mengakses web
3. node-postgres : Menghubungkan antara NodeJS dengan database PostgreSQL
4. dotenv : Membaca file .env sebagai Environment Variable dari Operating System

