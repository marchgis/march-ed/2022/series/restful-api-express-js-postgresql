const express = require('express')
const cors = require('cors')

const app = express()
const port = 3100

app.use(cors({
  origin: '*'
}))

app.get('/', (httpRequest, httpResponse) => {
  
  httpResponse.json({
    hello: "Dunia !"
  })
})

app.listen(port, () => {
  console.log(`Warga App berjalan di port ${port} `)
})
