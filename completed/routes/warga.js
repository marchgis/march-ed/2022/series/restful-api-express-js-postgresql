const express = require('express')
const router = express.Router()

const postWarga = require('../services/warga/post-warga')
const getWarga = require('../services/warga/get-warga')
const getWargaByID = require('../services/warga/get-warga-by-id')
const putWargaByID = require('../services/warga/put-warga-by-id')
const deleteWargaByID = require('../services/warga/delete-warga-by-id')

router.post("/", postWarga)
router.get("/", getWarga)
router.get("/:id([0-9a-z\-]+)", getWargaByID)
router.put("/:id([0-9a-z\-]+)", putWargaByID)
router.delete("/:id([0-9a-z\-]+)", deleteWargaByID)

module.exports = router
