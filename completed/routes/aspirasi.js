const express = require('express')
const router = express.Router()

router.get("/", (req, res) => {
  res.json([
    {
      id: 1,
      pengusul: 2,
      isi: "Ayo main pingpong",
    },
  ])
})

router.get("/:id([a-z0-9]+)", (req, res) => {
  res.json(
    {
      id: 1,
      pengusul: 2,
      isi: "Ayo main pingpong",
    },
  )
})

module.exports = router
