const pool = require('../../utils/db-pool')

module.exports = (httpRequest, httpResponse) => {

  if(httpRequest.query.rw !== null) {

    pool.query(
      `
        SELECT * FROM app.warga
        WHERE rw = $1
        ORDER BY kode_kelurahan, rw, rt
      `,
      [
        httpRequest.query.rw
      ],
      (dbError, dbResponse) => {
        if(dbError) throw dbError

        httpResponse.json(dbResponse.rows)
      }
    )

  } else {

    pool.query(
      `
        SELECT * FROM app.warga
        ORDER BY kode_kelurahan, rw, rt
      `,
      [],
      (dbError, dbResponse) => {
        if(dbError) throw dbError

        httpResponse.json(dbResponse.rows)
      }
    )

  }

}
