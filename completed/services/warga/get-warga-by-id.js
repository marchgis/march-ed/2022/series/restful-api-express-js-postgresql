const pool = require('../../utils/db-pool')

module.exports = (httpRequest, httpResponse) => {

  console.log(httpRequest.body)

  pool.query(
    `
      SELECT * FROM app.warga
      WHERE id = $1 
    `,
    [httpRequest.params.id],
    (dbError, dbResponse) => {
      if(dbError) throw dbError

      if(dbResponse.rowCount == 1) {
        httpResponse.json(dbResponse.rows[0])
      } else {
        httpResponse.json({})
      }
      
    }
  )
}
