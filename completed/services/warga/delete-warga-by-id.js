const pool = require('../../utils/db-pool')

module.exports = (httpRequest, httpResponse) => {

  pool.query(
    `
      DELETE FROM app.warga
      WHERE id = $1
    `,
    [
      httpRequest.params.id,
    ],
    (dbError, dbResponse) => {
      if(dbError) throw dbError

      httpResponse.json({
        deleted: httpRequest.params.id,
      })
      
    }
  )
}
