const express = require('express')
const cors = require('cors')
// const { Client } = require('pg')
// const jwt = require('jsonwebtoken')

const indexRoutes = require('./routes/index')
const aspirasiRoutes = require('./routes/aspirasi')
const wargaRoutes = require('./routes/warga')

require('dotenv').config()

const app = express()
const port = 3100

app.use(cors({
  origin: '*'
}))

app.use(express.json())

app.use("", indexRoutes)
app.use("/aspirasi", aspirasiRoutes)
app.use("/warga", wargaRoutes)

app.listen(port, () => {
  console.log(`Warga App berjalan di port ${port} `)
})
